package datos;

/**
 * Clase datos. Guarda los datos de clientes
 * @version 1.0
 * @author Alberto Soria Carrillo
 * @see
 *
 */
public class Datos {
    // Atributos
    private String nombre;
    private String apellido;
    private String genero;
    private double izquierdo;
    private double derecho;
    // Constructor
    public Datos(String nombre, String apellido, String genero, double izquierdo, double derecho) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.genero = genero;
        this.izquierdo = izquierdo;
        this.derecho = derecho;
    }
    // Getters and Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        apellido = apellido;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public double getIzquierdo() {
        return izquierdo;
    }

    public void setIzquierdo(double izquierdo) {
        this.izquierdo = izquierdo;
    }

    public double getDerecho() {
        return derecho;
    }

    public void setDerecho(double derecho) {
        this.derecho = derecho;
    }

    // Metodos toString
    @Override
    public String toString() {
        return "" + nombre + " " + apellido + " " + genero + "\n Izquierdo: " + izquierdo + ", derecho: " + derecho;
    }
}

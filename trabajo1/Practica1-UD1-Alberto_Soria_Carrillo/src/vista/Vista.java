package vista;

import datos.Datos;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import org.w3c.dom.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Clase Vista, ejecuta la venta y los elementos de la misma
 * @version 1.2
 * @author Alberto Soria Carrillo, Elena Jiménez Fuentes
 * @see datos.Datos,org.w3c.dom.DOMImplementation,org.w3c.dom.Document,org.w3c.dom.Document,org.w3c.dom.Element,
 *      javax.swing.*,javax.xml.parsers.DocumentBuilder,javax.xml.parsers.DocumentBuilderFactory,
 *      java.awt.*,java.awt.event.ActionEvent,java.awt.event.ActionListener,java.io.File,
 *      java.io.IOException,java.util.LinkedList,org.w3c.dom.*,
 *      javax.xml.parsers.ParserConfigurationException,javax.xml.transform.*,
 *      javax.xml.transform.dom.DOMSource,javax.xml.transform.stream.StreamResult,
 */
public class Vista extends JFrame implements ActionListener {
    // Atributos
    private JFrame frame;
    private JPanel panel1;
    private JTextField nombreTF;
    private JLabel nombreJL;
    private JTextField apellidoTF;
    private JLabel apellidoJL;
    private JComboBox listaCB;
    private JLabel listaJL;
    private JButton mostrarButton;
    private JButton anidarButton;
    private JRadioButton MujerRadioButton1;
    private JRadioButton hombreRadioButton;
    private JTextArea textArea1;
    private JTextField izquierdoTF;
    private JTextField derechoTF;
    private JLabel doptriasJL;
    private JLabel izquierdoJL;
    private JLabel derechoJL;
    private LinkedList<Datos> lista;
    private DefaultComboBoxModel<Datos> cliente;
    private String genero = "";
    private JMenuItem itemExportarXML;
    private JMenuItem itemImportarXML;

    /**
     * Crea la ventana y añade los elementos necesarios. Inicializa LinkedList y DefaultComboBoxModel
     */
    public void createUIComponents(){

        frame = new JFrame();
        panel1.setBackground(Color.black);
        panel1.setOpaque(false);
        frame.setTitle("Optica");
        frame.setBackground(Color.PINK);
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,350);
        frame.setResizable(false);
        menu();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        lista = new LinkedList<>();
        cliente = new DefaultComboBoxModel<>();
        listaCB.setModel(cliente);
        anidarListener();
    }

    /**
     * Añade los elementos listener
     */
    private void anidarListener() {
        mostrarButton.addActionListener(this);
        anidarButton.addActionListener(this);
        MujerRadioButton1.addActionListener(this);
        hombreRadioButton.addActionListener(this);
        itemExportarXML.addActionListener(this);
        itemImportarXML.addActionListener(this);
    }

    /**
     * Guarda los datos de los clientes
     */
    private void listaGuardar() {

        try {
            double izq = Double.parseDouble(izquierdoTF.getText());
            double der = Double.parseDouble(derechoTF.getText());

            lista.add(new Datos(nombreTF.getText(),apellidoTF.getText(), genero, izq, der));
        }catch(Exception e){
            textArea1.setText("Se debere introducir números en los apartados de graduación");
        }

    }

    /**
     * Crea el menu superior de la ventana y añade sus elementos
     */
    private void menu() {
   
            JMenuBar barra = new JMenuBar();
            JMenu menu = new JMenu("Datos");
            itemExportarXML = new JMenuItem("Exportar XML");
            itemImportarXML = new JMenuItem("Importar XML");

            menu.add(itemExportarXML);
            menu.add(itemImportarXML);

            barra.add(menu);
            frame.setJMenuBar(barra);
    }

    /**
     * Actualiza los elementos del combobox
     */
    private void refrescarComboBox() {
        cliente.removeAllElements();
        for (Datos persona : lista) {
            cliente.addElement(persona);
        }

    }

    /**
     * Importa los datos del fichero XML indicado
     * @param fichero
     */
    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            NodeList cliente = documento.getElementsByTagName("cliente");
            for (int i = 0; i < cliente.getLength(); i++) {
                Node coche = cliente.item(i);
                Element elemento = (Element) coche;

                String nombre = elemento.getElementsByTagName("nombre").item(0).getChildNodes().item(0).getNodeValue();
                String apellido = elemento.getElementsByTagName("apellidos").item(0).getChildNodes().item(0).getNodeValue();
                String gen = elemento.getElementsByTagName("genero").item(0).getChildNodes().item(0).getNodeValue();
                double izq = Double.parseDouble( elemento.getElementsByTagName("ojo_izquierdo").item(0).getChildNodes().item(0).getNodeValue());
                double der = Double.parseDouble( elemento.getElementsByTagName("ojo_derecho").item(0).getChildNodes().item(0).getNodeValue());

                lista.add(new Datos(nombre, apellido, gen, izq, der));
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }

    }

    /**
     * Exporta los datos del fichero XML indicado
     * @param fichero
     */
    private void exportarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            //Creo documento que representa arbol XML
            Document documento = dom.createDocument(null, "xml", null);

            //Creo el nodo raiz (coches) y lo añado al documento
            Element raiz = documento.createElement("clientes");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoCliente;
            Element nodoDatos;
            Text dato;

            for (Datos cliente : lista) {

                nodoCliente = documento.createElement("cliente");
                raiz.appendChild(nodoCliente);

                nodoDatos = documento.createElement("nombre");
                nodoCliente.appendChild(nodoDatos);

                dato = documento.createTextNode(cliente.getNombre());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("apellidos");
                nodoCliente.appendChild(nodoDatos);

                dato = documento.createTextNode(cliente.getApellido());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("genero");
                nodoCliente.appendChild(nodoDatos);

                dato = documento.createTextNode(cliente.getGenero());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("ojo_izquierdo");
                nodoCliente.appendChild(nodoDatos);

                dato = documento.createTextNode(String.valueOf(cliente.getIzquierdo()));
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("ojo_derecho");
                nodoCliente.appendChild(nodoDatos);

                dato = documento.createTextNode(String.valueOf(cliente.getDerecho()));
                nodoDatos.appendChild(dato);


            }

            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

    }

    /**
     * Realiza la accion indicada de ada boton
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource() == anidarButton){
            listaGuardar();
            refrescarComboBox();
        }

        if (e.getSource() == mostrarButton){
            Datos selec = (Datos) cliente.getSelectedItem();
            textArea1.setText(selec.toString());
        }

        if (e.getSource() == MujerRadioButton1){
            genero = "Mujer";
        }

        if (e.getSource() == hombreRadioButton){
            genero = "Hombre";
        }

        if (e.getSource() == itemExportarXML){
            JFileChooser selectorArchivo = new JFileChooser();
            int opcionSeleccionada = selectorArchivo.showSaveDialog(null);

            if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {

                File fichero = selectorArchivo.getSelectedFile();
                exportarXML(fichero);

            }
        }

        if (e.getSource() == itemImportarXML){
            JFileChooser selectorArchivo = new JFileChooser();
            int opcion = selectorArchivo.showOpenDialog(null);

            if (opcion == JFileChooser.APPROVE_OPTION) {

                File fichero = selectorArchivo.getSelectedFile();
                importarXML(fichero);
                refrescarComboBox();

            }
        }
    }
}

package vista.principal;

import vista.Vista;

/**
 * Clase main o de arranque del programa
 * @version 1.0
 * @author Alberto Soria Carrillo
 * @see vista.Vista
 */
public class Principal {

    // Main
    public static void main(String[] args) {

        Vista win = new Vista();

        win.createUIComponents();

    }
}
